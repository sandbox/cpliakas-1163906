<?php

/**
 * @file
 * CRUD functions for the Search Framework configuration settings.
 */

/**
 * @defgroup search_engine_crud Search Engine CRUD Functions
 * @{
 * CRUD functions for search engine configurations.
 */

/**
 * Gets all search engine configurations.
 *
 * @return array
 *   An associative array keyed by the unique name of the search engine
 *   configuration to the configuration options.
 */
function search_engine_load_all() {
  $engine_configs = &drupal_static(__FUNCTION__);
  if (NULL === $engine_configs) {
    ctools_include('export');
    $engine_configs = ctools_export_crud_load_all('search_engine');
  }
  return $engine_configs;
}

/**
 * Returns an empty search engine configuration object with defaults.
 *
 * @param string $engine_type
 *   The search engine type, which is the key in hook_search_engine_info()
 *   implementations.
 *
 * @return stdClass
 */
function search_engine_new($engine_type) {
  ctools_include('export');
  $engine_config = ctools_export_crud_new('search_engine');
  $engine_config->options = array(
    'label' => '',
    'name' => '',
    'engine_type' => $engine_type,
    'collections' => array(),
    'read_only' => 0,
    'index_on_cron' => 1,
  );
  return $engine_config;
}

/**
 * Loads a search engines's configuration settings.
 *
 * @param string $name
 *   The machine name of the search engine configuration.
 *
 * @return array|FALSE
 *   The configuration options, FALSE if the engine doesn't exist.
 */
function search_engine_load($name) {
  $engine_configs = &drupal_static(__FUNCTION__, array());
  if (!isset($engine_configs[$name])) {
    ctools_include('export');
    $engine_configs[$name] = ctools_export_crud_load('search_engine', $name);
  }
  return $engine_configs[$name] ?: FALSE;
}

/**
 * Saves a search engine's configuration settings.
 *
 * @param stdClass $engine_config
 *   The search engine configuration being saved.
 *
 * @return boolean
 */
function search_engine_save($engine_config) {
  $sucess = ctools_export_crud_save('search_engine', $engine_config);
  $args = array(
    '@type' => $engine_config->options['engine_type'],
    '@name' => $engine_config->name,
  );
  if ($sucess) {
    drupal_static_reset('search_engine_load');
    watchdog('search_framework', 'Connection to @type engine added: @name', $args);
  }
  else {
    watchdog('search_framework', 'Error connecting to @type engine: @name', $args, WATCHDOG_ERROR);
  }
  return $sucess;
}

/**
 * Deletes a search engine's configuration settings.
 *
 * @param stdClass $engine_config
 *   The search engine configuration being deleted.
 *
 * @return boolean
 */
function search_engine_delete($engine_config) {
  // @todo CTools doesn't return the status. Figure out how to get it.
  ctools_export_crud_delete('search_engine', $engine_config);
  drupal_static_reset('search_engine_load');
  $args = array(
    '@type' => $engine_config->options['engine_type'],
    '@name' => $engine_config->name,
  );
  watchdog('search_framework', 'Connection to @type engine deleted: @name', $args);
  return TRUE;
}

/**
 * Loads a collection's configuration settings.
 *
 * @param string $collection_name
 *   The machine name of the collection configuration.
 * @param string $engine_name
 *   The machine name of the search engine configuration.
 *
 * @return array|FALSE
 *   The configuration options, FALSE if the server doesn't exist.
 *
 * @see search_engine_load()
 */
function search_collection_load($collection_name, $engine_name) {
  $engine_config = search_engine_load($engine_name);
  if ($engine_config && isset($engine_config->options['collections'][$collection_name])) {
    return $engine_config->options['collections'][$collection_name];
  }
  return FALSE;
}

/**
 * Tests if the item name already exists.
 *
 * @return
 *   A boolean flagging whether the item exists.
 */
function search_collection_exists($name, &$element, &$form_state) {
  $engine_config = $element['#search_engine'];
  return isset($engine_config->options['collections'][$name]);
}

/**
 * @} End of "defgroup search_engine_crud".
 */

/**
 * @defgroup search_page_crud Search Page CRUD Functions
 * @{
 * CRUD functions for search pages configurations.
 */

/**
 * Gets all search page configurations.
 *
 * @return array
 *   An associative array keyed by the unique name of the search page
 *   configuration to the configuration options.
 */
function search_page_load_all() {
  $page_configs = &drupal_static(__FUNCTION__);
  if (NULL === $page_configs) {
    ctools_include('export');
    $page_configs = ctools_export_crud_load_all('search_page');
  }
  return $page_configs;
}

/**
 * Returns an empty search page configuration object with defaults.
 *
 * @return stdClass
 */
function search_page_new() {
  ctools_include('export');
  $page_config = ctools_export_crud_new('search_page');
  $page_config->options = array(
    'label' => '',
    'name' => '',
    'title' => '',
    'engine' => '',
    'url' => 'search/{engine}/{query}',
    'menu' => array(),
  );
  return $page_config;
}

/**
 * Loads a search page's configuration settings.
 *
 * @param string $name
 *   The machine name of the search page configuration.
 *
 * @return array|FALSE
 *   The configuration options, FALSE if the server doesn't exist.
 */
function search_page_load($name) {
  $page_configs = &drupal_static(__FUNCTION__, array());
  if (!isset($page_configs[$name])) {
    ctools_include('export');
    $page_configs[$name] = ctools_export_crud_load('search_page', $name);
  }
  return $page_configs[$name] ?: FALSE;
}

/**
 * Saves a search page's configuration settings.
 *
 * @param stdClass $page_config
 *   The search page configuration being saved.
 *
 * @return boolean
 */
function search_page_save($page_config) {
  $sucess = ctools_export_crud_save('search_page', $page_config);
  $args = array('@name' => $page_config->name);
  if ($sucess) {
    drupal_static_reset('search_page_load');
    watchdog('search_framework', 'Search page added: @name', $args);
  }
  else {
    watchdog('search_framework', 'Error adding search page: @name', $args, WATCHDOG_ERROR);
  }
  return $sucess;
}

/**
 * Deletes a search search page's configuration settings.
 *
 * @param stdClass $page_config
 *   The search page configuration being deleted.
 *
 * @return boolean
 */
function search_page_delete($page_config) {
  // @todo CTools doesn't return the status. Figure out how to get it.
  ctools_export_crud_delete('search_page', $page_config);
  drupal_static_reset('search_page_load');
  $args = array('@name' => $page_config->name);
  watchdog('search_framework', 'Search page deleted: @name', $args);
  return TRUE;
}

/**
 * @} End of "defgroup search_page_crud".
 */
