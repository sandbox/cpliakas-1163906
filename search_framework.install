<?php

/**
 * @file
 * Install, update, and uninstall functions for the Search Framework module.
 */

/**
 * Implements hook_schema().
 */
function search_framework_schema() {
  $schema = array();

  $schema['search_engine'] = array(
    'description' => 'Search Framework search engine configurations.',
    'export' => array(
      'key' => 'name',
      'identifier' => 'search_framework',
      'default hook' => 'default_search_engines',
      'api' => array(
        'owner' => 'search_framework',
        'api' => 'default_search_engines',
        'minimum_version' => 1,
        'current_version' => 1,
      ),
    ),
    'fields' => array(
      'name' => array(
        'description' => 'The machine readable name of the search engine configuration.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'options' => array(
        'description' => 'Serialized storage of search engine options.',
        'type' => 'blob',
        'serialize' => TRUE,
      ),
    ),
    'primary key' => array('name'),
  );

  $schema['search_page'] = array(
    'description' => 'Search Framework search page configurations.',
    'export' => array(
      'key' => 'name',
      'identifier' => 'search_framework',
      'default hook' => 'default_search_pages',
      'api' => array(
        'owner' => 'search_framework',
        'api' => 'default_search_pages',
        'minimum_version' => 1,
        'current_version' => 1,
      ),
    ),
    'fields' => array(
      'name' => array(
        'description' => 'The machine readable name of the search page configuration.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'options' => array(
        'description' => 'Serialized storage of seach page options.',
        'type' => 'blob',
        'serialize' => TRUE,
      ),
    ),
    'primary key' => array('name'),
  );

  return $schema;
}

/**
 * Implements hook_uninstall().
 */
function search_framework_uninstall() {
  // Nothing to do...
}