<?php

/**
 * @file
 * Hooks provided by the Search Framework module.
 */

use Search\Framework\Indexer;

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Defines search engines.
 */
function hook_search_engine_info() {
  $engine_info = array();


  return $engine_info;
}

/**
 * Alters search engines.
 */
function hook_search_engine_info_alter(array &$engine_info) {

}

/**
 * Defines collections.
 */
function hook_search_collection_info() {
  $collection_info = array();



  return $collection_info;
}

/**
 *
 */
function hook_search_collection_info_alter(array &$collection_info) {

}

/**
 *
 */
function hook_search_init_indexer(Indexer $indexer, $engine_config) {

}

/**
 * @} End of "addtogroup hooks".
 */
