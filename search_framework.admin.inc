<?php

/**
 * @file
 */

/**
 * The regex pattern for field arguments.
 */
define('SEARCH_FRAMEWORK_ARGUMENT_PATTERN', '@^{field:([-_a-zA-Z0-9]+)}$@');

/**
 * @defgroup search_engine_admin Search Engine Admin Pages & Forms
 * @{
 * Search Engine administrative pages, forms, and form handlers.
 */

/**
 *
 */
function search_engine_overview_page() {
  $build = array();

  $header = array(
    'label' => t('Service Name'),
    'connection' => t('Connection'),
    'operations' => t('Operations'),
  );

  $rows = array();
  foreach (search_engine_load_all() as $engine_name => $engine_config) {
    $base_path = 'admin/config/search/search-framework/engine/' . $engine_name;
    $engine_info = search_engine_info_load($engine_config->options['engine_type']);

    $row = array(
      'label' => check_plain($engine_config->options['label']),
      'connection' => $engine_info['engine overview']($engine_config),
    );

    $operations = array();
    $operations[] = array(
      'title' => t('Edit'),
      'href' => $base_path . '/edit',
    );
    $operations[] = array(
      'title' => t('Collections'),
      'href' => $base_path . '/collection',
    );
    $operations[] = array(
      'title' => t('Status'),
      'href' => $base_path . '/status',
    );
    $operations[] = array(
      'title' => t('Delete'),
      'href' => $base_path . '/delete',
    );

    $row['operations'] = array(
      'data' => array(
        '#theme' => 'links__node_operations',
        '#links' => $operations,
        '#attributes' => array('class' => array('links', 'inline')),
      ),
    );

    $rows[] = $row;
  }

  $build['engines'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#empty' => t('There are no search engine connections. Add one by clicking the link above.'),
  );

  return $build;
}

/**
 *
 */
function search_engine_type_page() {
  $build = array();

  $variables = array(
    'info' => search_engine_info_load_all(),
    'base_path' => 'admin/config/search/search-framework/engine',
    'empty_message' => t('There are no available search engine types.'),
  );
  $build['engine_list'] = array(
    '#markup' => theme('search_framework_type_list', $variables),
  );

  $build['cancel_link'] = array(
    '#markup' => l(t('Cancel'), 'admin/config/search/search-framework/engine'),
    '#weight' > 99,
  );

  return $build;
}

/**
 *
 */
function search_engine_form($form, &$form_state, $engine_config = NULL) {
  $form['#is_new'] = empty($engine_config);

  if ($form['#is_new']) {
    // Pull the engine type form the path.
    $engine_config = search_engine_new(arg(6));
  }

  $form['#search_engine'] = $engine_config;

  $form['label'] = array(
    '#title' => t('Label'),
    '#type' => 'textfield',
    '#default_value' => $engine_config->options['label'],
    '#description' => t('A label that describes this search engine, e.g. "Local Solr", "Elasticsearch Cluster", etc.'),
    '#required' => TRUE,
    '#maxlength' => 255,
    '#size' => 30,
  );

  $form['name'] = array(
    '#type' => 'machine_name',
    '#default_value' => $engine_config->options['name'],
    '#maxlength' => 32,
    '#machine_name' => array(
      'exists' => 'search_engine_load',
      'source' => array('label'),
    ),
    '#disabled' => !$form['#is_new'],
    '#description' => t('The machine readable name of the search engine configuration. This value can only contain letters, numbers, and underscores.'),
  );

  $form['collections'] = array(
    '#type' => 'value',
    '#value' => $engine_config->options['collections'],
  );

  $form['engine_type'] = array(
    '#type' => 'value',
    '#value' => $engine_config->options['engine_type'],
  );

  $form['endpoints'] = array(
    '#type' => 'value',
    '#value' => array(),
  );

  $form['advanced_title'] = array(
    '#type' => 'item',
    '#title' => t('Advanced Settings'),
    '#weight' => 10,
  );

  $form['advanced'] = array(
    '#type' => 'vertical_tabs',
    '#weight' => 10,
  );

  $form['indexing'] = array(
    '#type' => 'fieldset',
    '#title' => t('Indexing'),
    '#group' => 'advanced',
  );

  $form['indexing']['read_only'] = array(
    '#type' => 'checkbox',
    '#title' => t('Read only'),
    '#description' => t('Update and delete operatios will not be performed for this search engine.'),
    '#group' => 'advanced',
    '#default_value' => $engine_config->options['read_only'],
  );

  $form['indexing']['index_on_cron'] = array(
    '#type' => 'checkbox',
    '#title' => t('Index on cron'),
    '#description' => t('Perform indexing operations on cron runs.'),
    '#states' => array(
      'visible' => array(
        ':input[name="read_only"]' => array('checked' => FALSE),
      ),
    ),
    '#group' => 'advanced',
    '#default_value' => $engine_config->options['index_on_cron'],
  );

  $form['actions'] = array(
    '#type' => 'actions',
    '#weight' > 99,
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  $form['actions']['cancel'] = array(
    '#markup' => l(t('Cancel'), 'admin/config/search/search-framework/engine'),
  );

  $form['#validate'] = array();
  $form['#submit'] = array('search_engine_form_submit');

  $engine_info = search_engine_info_load($engine_config->options['engine_type']);

  // Apply the defaults.
  if ($form['#is_new']) {
    $engine_config->options += $engine_info['engine defaults'];
  }

  $engine_info['engine settings']($form, $form_state, $engine_config);

  return $form;
}

/**
 *
 */
function search_engine_form_submit($form, &$form_state) {
  $engine_config = &$form['#search_engine'];

  form_state_values_clean($form_state);
  $engine_config->name = $form_state['values']['name'];
  $engine_config->options = array_merge($engine_config->options, $form_state['values']);

  try {
    if (!search_engine_save($engine_config)) {
      throw new Exception(t('Error saving search engine configuration.'));
    }

    $form_state['redirect'] = 'admin/config/search/search-framework/engine';
    $message = $form['#is_new'] ? t('Configuration saved.') : t('Configuration updated.');
    drupal_set_message($message);

  }
  catch (Exception $e) {
    form_set_error(NULL, $e->getMessage());
    watchdog_exception('search_framework', $e);
  }
}

/**
 *
 */
function search_engine_status_page($engine_config) {
  $build = array();

  $engine_info = search_engine_info_load($engine_config->options['engine_type']);
  if ($engine_info) {
    $build += $engine_info['engine status']($engine_config);
  }
  else {
    // @todo Handle Error
  }

  return $build;
}

/**
 *
 */
function search_engine_delete_form($form, &$form_state, $engine_config) {
  $form['#search_engine'] = $engine_config;

  // @todo Add options to handle the related search pages.

  $args = array('%label' => $engine_config->options['label']);
  $prompt = t('Are you sure you want to remove the connection to the %label search engine?', $args);
  $cancel_path = 'admin/config/search/search-framework';
  $warning = t('This action cannot be undone.');

  return confirm_form($form, $prompt, $cancel_path, $warning, t('Delete'), t('Cancel'));
}

/**
 *
 */
function search_engine_delete_form_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    $engine_config = $form['#search_engine'];
    $args = array('%label' => $engine_config->options['label']);
    if (search_engine_delete($engine_config)) {
      drupal_set_message(t('Connection to the %label search engine has been removed.', $args));
    } else {
      drupal_set_message(t('Error removing connection to the %label search engine.', $args), 'error');
    }
  }
  $form_state['redirect'] = 'admin/config/search/search-framework';
}

/**
 * @} End of "defgroup search_engine_admin".
 */

/**
 * @defgroup search_framework_page_admin Search Page Admin Pages & Forms
 * @{
 * Search Page administrative pages, forms, and form handlers.
 */

/**
 *
 */
function search_page_overview_page() {
  $build = array();

  $engine_configs = search_engine_load_all();
  if (!$engine_configs) {
    return array(
      '#markup' => t('At least one connection to a search engine is required to create a search page.'),
    );
  }

  $header = array(
    'label' => t('Name'),
    'url' => t('URL'),
    'engine' => t('Search Engine'),
    'operations' => t('Operations'),
  );

  $rows = array();
  foreach (search_page_load_all() as $page_name => $page_config) {
    $base_path = 'admin/config/search/search-framework/search-page/' . $page_name;
    $engine_config = $engine_configs[$page_config->options['engine']];
    $menu = $page_config->options['menu'];

    $row = array(
      'label' => check_plain($page_config->options['label']),
      'url' => l(str_replace(array('%menu_tail', '%'), '*', $menu['item']), $menu['path']),
      'server' => check_plain($engine_config->options['label']),
    );

    $operations = array();
    $operations[] = array(
      'title' => t('Edit'),
      'href' => $base_path . '/edit',
    );
    $operations[] = array(
      'title' => t('Delete'),
      'href' => $base_path . '/delete',
    );

    $row['operations'] = array(
      'data' => array(
        '#theme' => 'links__node_operations',
        '#links' => $operations,
        '#attributes' => array('class' => array('links', 'inline')),
      ),
    );

    $rows[] = $row;
  }

  $build['search_pages'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#empty' => t('No search pages have been created. Add one by clicking the link above.'),
  );

  return $build;
}

/**
 *
 */
function search_page_form($form, &$form_state, $page_config = NULL) {
  $form['#is_new'] = empty($page_config);

  if ($form['#is_new']) {
    $page_config = search_page_new();
  }

  $form['#search_page'] = $page_config;

  $form['label'] = array(
    '#title' => t('Label'),
    '#type' => 'textfield',
    '#default_value' => $page_config->options['label'],
    '#description' => t('A label used on administrative pages that describes this search page, e.g. "Site search", "Blog search", etc.'),
    '#required' => TRUE,
    '#maxlength' => 255,
    '#size' => 30,
  );

  $form['name'] = array(
    '#type' => 'machine_name',
    '#default_value' => $page_config->options['name'],
    '#maxlength' => 32,
    '#machine_name' => array(
      'exists' => 'search_page_load',
      'source' => array('label'),
    ),
    '#disabled' => !$form['#is_new'],
    '#description' => t('The machine readable name of the search page configuration. This value can only contain letters, numbers, and underscores.'),
  );

  $form['engine'] = array(
    '#title' => t('Search Engine Connection'),
    '#type' => 'select',
    '#options' => search_engine_options(),
    '#default_value' => $page_config->options['engine'],
    '#description' => t('The search engine that will perform the search queries on this page.'),
    '#required' => TRUE,
  );

  $form['title'] = array(
    '#title' => t('Search Page Title'),
    '#type' => 'textfield',
    '#default_value' => $page_config->options['title'],
    '#description' => t('The title of the search page.'),
    '#required' => TRUE,
  );

  // @todo Create a theme function for this.
  $description = t('The relative URL of the search page.');
  $description .= '<p>' . t('Replacement patterns:') . '</p>';
  $description .= '<ul>';
  $description .= '<li><code>{query}</code>: Location of the keywords in the URL, e.g. <code>search/{query}</code> or <code>search?query={query}</code>.</li>';
  $description .= '<li><code>{engine}</code>: Replaced with the machine name of the search engine connection selected above.</li>';
  $description .= '<li><code>{field:*}</code>: Adds a dynamic (contextual) filter for the specified field.</li>';
  $description .= '</ul>';

  $form['url'] = array(
    '#title' => t('Search Page URL'),
    '#type' => 'textfield',
    '#default_value' => $page_config->options['url'],
    '#description' => $description,
    '#required' => TRUE,
  );

  $form['menu'] = array(
    '#type' => 'value',
    '#value' => $page_config->options['menu'],
  );

  $form['actions'] = array(
    '#type' => 'actions',
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  $form['actions']['cancel'] = array(
    '#markup' => l(t('Cancel'), 'admin/config/search/search-framework/search-page'),
  );

  $form['#validate'] = array('search_page_form_validate');
  $form['#submit'] = array('search_page_form_submit');

  return $form;
}

/**
 *
 */
function search_page_form_validate($form, &$form_state) {
  $url_error = $keywords_in_path = $keywords_in_query = FALSE;
  $field_arguments = array();

  $url = search_framework_parse_url($form_state['values']['url']);
  $url_valid = search_framework_validate_url($url, $url_error);

  if ($url_valid) {

    // Total number of menu parts.
    $num_parts = count($url['path']) - 1;

    // Builds the path parts.
    $path_parts = array();
    foreach ($url['path'] as $index => $path_part) {
      if (preg_match(SEARCH_FRAMEWORK_ARGUMENT_PATTERN, $path_part, $match)) {
        $path_parts[] = '%';
        $field_arguments[$match[1]] = array(
          'type' => 'path',
          'index' => $index,
          'field' => $match[1],
        );
      }
      elseif ('{engine}' == $path_part) {
        $path_parts[] = $form_state['values']['engine'];
      }
      elseif ($path_part != '{query}') {
        $path_parts[] = $path_part;
      }
      elseif ($keywords_in_path) {
        $url_error = t('The keyword location can only be specified once.');
        break;
      }
      elseif ($index != $num_parts) {
        $url_error = t('The keyword location must be the last item in the path.');
        break;
      }
      else {
        $path_parts[] = '%menu_tail';
        $keywords_in_path = TRUE;
      }
    }

    // Find the keywords in the query string.
    $keywords_variable = FALSE;
    foreach ($url['query'] as $variable => $value) {
      if (preg_match(SEARCH_FRAMEWORK_ARGUMENT_PATTERN, $value, $match)) {
        $field_arguments[$match[1]] = array(
          'type' => 'query',
          'variable' => $variable,
          'field' => $match[1],
        );
      }
      elseif ('{query}' == $value) {
        if (!$keywords_in_path && !$keywords_in_query) {
          $keywords_variable = $variable;
        }
        else {
          $url_error = t('The keyword localtion can only be defined once.');
          break;
        }
      }
      else {
        $url_error = t('Hard coded query string variables are not allowed.');
      }
    }
  }

  if (!$url_error) {

    $menu = array(
      'item' => join('/', $path_parts),
      'field arguments' => $field_arguments,
      'keywords in path' => $keywords_in_path,
    );
    if ($keywords_in_path) {
      $menu['keywords index'] = count($url['path']) - 1;
      $menu['tab root'] = str_replace('/%menu_tail', '/%', $menu['item']);
      $menu['tab parent'] = str_replace('/%menu_tail', '', $menu['item']);
      $menu['path'] = $menu['tab parent'];
    }
    else {
      $menu['keywords variable'] = $variable;
      $menu['path'] = $menu['item'];
    }

    // Trigger a menu rebuild if tha page is new or something changed.
    if ($form['#is_new'] || (isset($form['menu']['#value']['item']) && $menu['item'] !=$form['menu']['#value']['item'])) {
      variable_set('menu_rebuild_needed', TRUE);
    }

    form_set_value($form['menu'], $menu, $form_state);
  }
  else {
    form_set_error('url', $url_error);
  }
}

/**
 *
 */
function search_page_form_submit($form, &$form_state) {
  $page_config = &$form['#search_page'];

  form_state_values_clean($form_state);
  $page_config->name = $form_state['values']['name'];
  $page_config->options = array_merge($page_config->options, $form_state['values']);

  try {
    if (!search_page_save($page_config)) {
      throw new Exception(t('Error saving search page connection.'));
    }

    $form_state['redirect'] = 'admin/config/search/search-framework/search-page';
    $message = $form['#is_new'] ? t('Configuration saved.') : t('Configuration updated.');
    drupal_set_message($message);
  }
  catch (Exception $e) {
    form_set_error(NULL, $e->getMessage());
    watchdog_exception('search_framework', $e);
  }
}

/**
 *
 */
function search_page_delete_form($form, &$form_state, $page_config) {
  $form['#search_page'] = $page_config;

  $args = array('%label' => $page_config->options['label']);
  $prompt = t('Are you sure you want to remove the the %label search page?', $args);
  $cancel_path = 'admin/config/search/search-framework/search-page';
  $warning = t('This action cannot be undone.');

  return confirm_form($form, $prompt, $cancel_path, $warning, t('Delete'), t('Cancel'));
}

/**
 *
 */
function search_page_delete_form_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    $page_config = $form['#search_page'];
    $args = array('%label' => $page_config->options['label']);
    if (search_page_delete($page_config)) {
      drupal_set_message(t('The %label search page has been removed.', $args));
    } else {
      drupal_set_message(t('Error removing the %label search page.', $args), 'error');
    }
  }
  $form_state['redirect'] = 'admin/config/search/search-framework/search-page';
}

/**
 * @} End of "defgroup search_page_admin".
 */

/**
 * @defgroup search_framework_url Search Page URL Parsers & Validators
 * @{
 * Functions that parse the URL into its various parts and validate the syntax.
 */

/**
 *
 */
function search_framework_parse_url($url) {
  $parsed_url = parse_url($url);
  if (!$parsed_url) {
    return FALSE;
  }

  // Parse the path parts.
  if (isset($parsed_url['path'])) {
    // Split path by "/", filter empty parts, re-key so parts are zero-based.
    $parsed_url['path'] = array_values(array_filter(explode('/', $parsed_url['path'])));
  }
  else {
    $parsed_url['path'] = array();
  }

  // Parse the query string into associative array.
  if (isset($parsed_url['query'])) {
    parse_str($parsed_url['query'], $query);
    $parsed_url['query'] = $query;
  }
  else {
    $parsed_url['query'] = array();
  }

  return $parsed_url;
}

/**
 *
 */
function search_framework_validate_url($url, &$errstr = NULL) {
  if (!$url) {
    $errstr = t('Path not valid.');
  }
  elseif (!empty($url['scheme']) || !empty($url['host'])) {
    $errstr = t('Only relative paths are allowed.');
  }
  elseif (empty($url['path'])) {
    $errstr = t('Path to search page is required, e.g. "search", "search/site".');
  }
  elseif (isset($url['query']['q'])) {
    $errstr = t('The query string variable "@var" cannot be used to pass the keywords.', array('@var' => 'q'));
  }
  elseif (isset($url['query']['page'])) {
    $errstr = t('The query string variable "@var" cannot be used to pass the keywords.', array('@var' => 'page'));
  }
  elseif (isset($url['query']['destination'])) {
    $errstr = t('The query string variable "@var" cannot be used to pass the keywords.', array('@var' => 'destination'));
  }
  elseif (!in_array('{query}', $url['path']) && !in_array('{query}', $url['query'])) {
    $errstr = t('The keyword location in the URL must be specified by using the <code>{query}</code> token.');
  }
  else {
    $errstr = '';
  }
  return empty($errstr);
}

/**
 * @} End of "defgroup search_framework_url".
 */

/**
 * @defgroup search_framework_collection_admin Collection Admin Pages & Forms
 * @{
 * Collection administrative pages, forms, and form handlers.
 */

/**
 *
 */
function search_collection_overview_page($engine_config) {
  $build = array();

  $header = array(
    'label' => t('Name'),
    'operations' => t('Operations'),
  );

  $rows = array();
  foreach ($engine_config->options['collections'] as $collection_name => $collection_config) {
    $base_path = 'admin/config/search/search-framework/engine/' . $engine_config->name . '/collection/' . $collection_name;

    $row = array(
      'label' => check_plain($collection_config['label']),
    );

    $operations = array();
    $operations[] = array(
      'title' => t('Edit'),
      'href' => $base_path . '/edit',
    );
    $operations[] = array(
      'title' => t('Delete'),
      'href' => $base_path . '/delete',
    );

    $row['operations'] = array(
      'data' => array(
        '#theme' => 'links__node_operations',
        '#links' => $operations,
        '#attributes' => array('class' => array('links', 'inline')),
      ),
    );

    $rows[] = $row;
  }

  $build['collections'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#empty' => t('No collections have been created. Add one by clicking the link above.'),
  );

  return $build;
}

/**
 *
 */
function search_collection_type_page($engine_config) {
  $build = array();

  $variables = array(
    'info' => search_collection_info_load_all(),
    'base_path' => 'admin/config/search/search-framework/engine/' . $engine_config->name . '/collection',
    'empty_message' => t('There are no available collection types.'),
  );
  $build['collection_list'] = array(
    '#markup' => theme('search_framework_type_list', $variables),
  );

  $build['cancel_link'] = array(
    '#markup' => l(t('Cancel'), 'admin/config/search/search-framework/engine/' . $engine_config->name . '/collection'),
    '#weight' => 99,
  );

  return $build;
}

/**
 *
 */
function search_collection_form($form, &$form_state, $engine_config, $collection_config = NULL) {
  $form['#is_new'] = empty($collection_config);

  if ($form['#is_new']) {
    $collection_config = array(
      'name' => '',
      'label' => '',
      'collection_type' => arg(8),
    );
  }

  $form['#search_engine'] = $engine_config;

  $form['label'] = array(
    '#title' => t('Label'),
    '#type' => 'textfield',
    '#default_value' => $collection_config['label'],
    '#description' => t('A label that describes this collection, e.g. "Drupal feeds", "News feeds".'),
    '#required' => TRUE,
    '#maxlength' => 255,
    '#size' => 30,
  );

  $form['name'] = array(
    '#type' => 'machine_name',
    '#default_value' => $collection_config['name'],
    '#maxlength' => 32,
    '#machine_name' => array(
      'exists' => 'search_collection_exists',
      'source' => array('label'),
    ),
    '#disabled' => !$form['#is_new'],
    '#description' => t('The machine readable name of the search engine configuration. This value can only contain letters, numbers, and underscores.'),
    '#search_engine' => $engine_config,
  );

  $form['collection_type'] = array(
    '#type' => 'value',
    '#value' => $collection_config['collection_type'],
  );

  $form['actions'] = array(
    '#type' => 'actions',
    '#weight' > 99,
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  $cancel_path = 'admin/config/search/search-framework/engine/' . $engine_config->name . '/collection';
  $form['actions']['cancel'] = array(
    '#markup' => l(t('Cancel'), $cancel_path),
  );

  $form['#validate'] = array();
  $form['#submit'] = array('search_collection_form_submit');

  $collection_info = search_collection_info_load($collection_config['collection_type']);

  // Apply the defaults.
  if ($form['#is_new']) {
    $collection_config += $collection_info['collection defaults'];
  }

  $collection_info['collection settings']($form, $form_state, $collection_config);

  return $form;
}

/**
 *
 */
function search_collection_form_submit($form, &$form_state) {
  $engine_config = &$form['#search_engine'];

  form_state_values_clean($form_state);
  $name = $form_state['values']['name'];
  $engine_config->options['collections'][$name] = $form_state['values'];

  try {
    if (!search_engine_save($engine_config)) {
      throw new Exception(t('Error saving data collection configuration.'));
    }

    $form_state['redirect'] = 'admin/config/search/search-framework/engine/' . $engine_config->name . '/collection';
    $message = $form['#is_new'] ? t('Configuration saved.') : t('Configuration updated.');
    drupal_set_message($message);

  }
  catch (Exception $e) {
    form_set_error(NULL, $e->getMessage());
    watchdog_exception('search_framework', $e);
  }
}

/**
 *
 */
function search_collection_delete_form($form, &$form_state, $engine_config, $collection_config) {
  $form['#search_engine'] = $engine_config;
  $form['#search_collection'] = $collection_config;

  // @todo Add options to handle the related search pages.

  $args = array('%label' => $collection_config['label']);
  $prompt = t('Are you sure you want to remove the %label collection?', $args);
  $cancel_path = 'admin/config/search/search-framework/engine/' . $engine_config->name . '/collection';
  $warning = t('This action cannot be undone.');

  return confirm_form($form, $prompt, $cancel_path, $warning, t('Delete'), t('Cancel'));
}

/**
 *
 */
function search_collection_delete_form_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {

    $engine_config = $form['#search_engine'];
    $collection_config = $form['#search_collection'];
    $collection_name = $collection_config['name'];

    $args = array('%label' => $collection_config['label']);
    unset($engine_config->options['collections'][$collection_name]);
    if (search_engine_save($engine_config)) {
      drupal_set_message(t('The %label collection has been removed.', $args));
    } else {
      drupal_set_message(t('Error removing the %label collection.', $args), 'error');
    }
  }

  $form_state['redirect'] = 'admin/config/search/search-framework/engine/' . $engine_config->name;
}

/**
 * @} End of "defgroup search_framework_collection".
 */

/**
 *
 */
function search_engine_options() {
  $options = array();
  foreach (search_engine_load_all() as $engine_name => $engine_config) {
    $options[$engine_name] = check_plain($engine_config->options['label']);
  }
  return $options;
}

/**
 *
 * @ingroup themeable
 */
function theme_search_framework_type_list($variables) {
  $output = '';

  if ($variables['info']) {

    $output = '<dl>';
    foreach ($variables['info'] as $type => $info) {
      $path = $variables['base_path'] . '/add/' . $type;
      $output .= '<dt>' . l($info['label'], $path) . '</dt>';
      $output .= '<dd>' . filter_xss_admin($info['description']) . '</dd>';
    }
    $output .= '</dl>';

  }
  else {
    $output = '<p>' . filter_xss_admin($variables['empty_message']) . '</p>';
  }

  return $output;
}
