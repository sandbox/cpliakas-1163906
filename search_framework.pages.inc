<?php

/**
 *
 * @file
 */

/**
 * Page callback; Search page callback for the Search Framework.
 *
 * @param string $page_name
 *   The machine name of the search page confguration.
 * @param string $keywords
 *   The search keywords passed through the path, if any.
 */
function search_page_view($page_name, $keywords = '') {
  $build = array();
  try {

    // Loads the search page configuration.
    $page_config = search_page_load($page_name);
    if (!$page_config) {
      $message = t('Search page "@page_name" not valid.', array('@page_name' => $page_name));
      throw new \InvalidArgumentException($message);
    }

    // Extract the keywords from the query string if applicable.
    $menu = $page_config->options['menu'];
    if (!$menu['keywords in path']) {
      $variable = $menu['keywords variable'];
      $keywords = (isset($_GET[$variable])) ? (string) $_GET[$variable] : '';
    }

    //$resultset = search_page_execute($page_config, $keywords);
    $resultset = array();

    $build['search_form'] = drupal_get_form('search_framework_search_form', $page_config, $keywords);

    $build['search_results'] = array(
      '#theme' => 'search_results',
      '#results' => search_results_build($resultset),
      '#module' => 'search_framework_search_page',
    );

  }
  catch (\InvalidArgumentException $e) {
    watchdog('search_framework_search_page', $e);
    drupal_not_found();
  }
  catch (\RuntimeException $e) {
    watchdog('search_framework_search_page', $e);
    $is_admin = user_access('administer solr servers');
    $message = $is_admin ? check_plain($e->getMessage()) : t('Search is temporarily unavailable. If the problem persists, please contact the site administrator.');
    drupal_set_message($message, 'error');
  }

  return $build;
}

/**
 *
 * @return array
 */
function search_results_build($resultset) {
  $results = array();
  foreach ($resultset as $document) {
    $results[] = array(
      'link' => isset($document->url) ? $document->url : '',
      'title' => htmlspecialchars_decode($document->label, ENT_QUOTES),
      'score' => $document->score,
      'snippets' => '',
      'snippet' => '',
      'fields' => array(),
      'entity_type' => $document->entity_type,
      'bundle' => isset($document->bundle) ? $document->bundle : '',
    );
  }
  return $results;
}

/**
 * Search page form for Solarium searches.
 *
 * @param stdClass $page_config
 *   The search page configuration.
 * @param string $keywords
 *   The keywords passed by the user.
 *
 * @ingroup forms
 *
 * @see search_form()
 */
function search_framework_search_form($form, &$form_state, $page_config, $keywords = '') {

  // Load the core Search CSS file, use the core search module's classes.
  drupal_add_css(drupal_get_path('module', 'search') . '/search.css');
  $form['#id'] = 'search-form';
  $form['#attributes']['class'][] = 'search-form';

  $form['#search_page'] = $page_config;

  $form['basic'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('container-inline')),
  );

  // @todo Make this a setting.
  $prompt = t('Enter terms');
  $form['basic']['keys'] = array(
    '#type' => 'textfield',
    '#title' => check_plain($prompt),
    '#default_value' => $keywords,
    '#size' => $prompt ? 20 : 40,
    '#maxlength' => 255,
  );

  // @todo Make this a setting.
  $button_text = t('Search');
  $form['basic']['submit'] = array(
    '#type' => 'submit',
    '#value' => check_plain($button_text),
  );

  $form['#submit'] = array('search_framework_search_form_submit');

  return $form;
}

/**
 * Form submission handler for search_framework_search_page_form().
 */
function search_framework_search_form_submit(&$form, &$form_state) {
  $page_config = $form['#search_page'];
  $menu = $page_config->options['menu'];

  $path_parts = explode('/', $menu['path']);
  $query = array();

  // Replace the field arguments with the passed values.
  foreach ($menu['field arguments'] as $field_name => $argument_info) {
    if ('path' == $argument_info['type']) {
      $index = $argument_info['index'];
      $path_parts[$index] = arg($index);
    }
    elseif ('query' == $argument_info['type']) {
      $variable = $argument_info['variable'];
      if (isset($_GET[$variable])) {
        $query[$variable] = (string) $_GET[$variable];
      }
    }
    else {
      $args = array('@type' => $argument_info['type']);
      watchdog('search_framework', 'Invalid field argument type: @type', $args, WATCHDOG_ERROR);
    }
  }

  // Appends the keywords to the path or sets it as a query string variable.
  if (strlen($form_state['values']['keys']) > 0) {
    if ($menu['keywords in path']) {
      $path_parts[] = $form_state['values']['keys'];
    }
    else {
      $variable = $menu['keywords variable'];
      $query[$variable] = $form_state['values']['keys'];
    }
  }

  $form_state['redirect'] = array(join('/', $path_parts), array('query' => $query));
}
